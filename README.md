#Configuration in
config.conf<br>
look into the <b>config.conf.example</b> for configuration details.<br>

examle configfile:<br>
config.conf.example<br>

#start - config.conf will be processed<br>
./wetterturnier.py

#start with debugmessages
./wetterturnier.py -v

#other params:
print 'wetterturnier.py -v -d 20130901 -s 11035'

print 'wetterturnier.py -d 20130901 -s 11035'

print 'wetterturnier.py -s 11035'

print 'wetterturnier.py --outputdebugmessages --date 20130901 --station 11035'

print 'wetterturnier.py --date 20130901 --station 11035'

print 'wetterturnier.py --station 11035'

print 'wetterturnier.py -t 2300'
print 'wetterturnier.py --time 2300'

print 'wetterturnier.py --outputpath output'
print 'wetterturnier.py -p output'

print '!! only stations which are defined in the config.conf work - that means every station needs a defined tablename !!'
