#!/usr/bin/env /opt/python27/bin/python2.7
# -*- coding: utf-8 -*-

# wetterturnier.py
#
# purpose:  Wetterturnier.de
# author:   Szabolcs KÖLLÖ
# e-mail:   szabolcs.koelloe@uibk.ac.at
# web:      http://imgi.uibk.ac.at/
# created:  12-Aug-2013
# modified:


import databaseSYBASE as database
import sys
import os

import getopt #www.tutorialspoint.com/python/python_command_line_arguments.htm

import datetime
import dateFunctions
import synopcode
import miscFunctions

__author__ = 'szabo'

EMPTYSTRING = ""

if __name__ == "__main__":

    debug_flag = False
    dontUseSaturdaySunday = False

    config = {}
    execfile("/home/c707/c7071039/wetterturnier_hourly/config.conf", config)

    connection = database.create_con(config)
    stations = config["stations"]
    
    time = synopcode.EMPTYSTRING
    ofile = synopcode.EMPTYSTRING
    opath = "output"
    processingDate = None

    try:
        opts, args = getopt.getopt(sys.argv[1:],"vd:s:t:o:p:",["outputdebugmessages=","date=","station=","time=","outputfile=","outputpath="])
    except getopt.GetoptError:
        print 'wetterturnier.py -v -d 20130901 -s 11035'
        print 'wetterturnier.py -d 20130901 -s 11035'
        print 'wetterturnier.py -s 11035'
        print 'wetterturnier.py --outputdebugmessages --date 20130901 --station 11035'
        print 'wetterturnier.py --date 20130901 --station 11035'
        print 'wetterturnier.py --station 11035'
        print 'wetterturnier.py --outputpath output'
        print '!! only stations which are defined in the config.conf work - that means every station needs a defined tablename !!'
        sys.exit(2)

    for opt, arg in opts:
        if opt in ("-v", "--outputdebugmessages"):
            debug_flag = True

        if opt in ("-d", "--date"):
            processingDate = datetime.date(int(arg[0:4]), int(arg[4:6]), int(arg[6:8]))
            dontUseSaturdaySunday = True

        if opt in ("-s", "--station"):
            stations = [arg]
            
        if opt in ("-t", "--time"):
            time = [arg]
        
        if opt in ("-o", "--outputfile"):
            ofile = arg
            
        if opt in ("-p", "--outputpath"):
            opath = arg

    outputline = synopcode.EMPTYSTRING

    if not dontUseSaturdaySunday:
        saturday = dateFunctions.get_last_saturday(datetime.date.today())
        sunday = dateFunctions.get_last_sunday(datetime.date.today())

        saturdayCorrected = dateFunctions.get_corrected_last_saturday(datetime.date.today())
        sundayCorrected = dateFunctions.get_corrected_last_sunday(datetime.date.today())

    for station in stations: # stations
        # BEGIN LOOP for stations
        tableNameString = config["s" + str(station)]
        
        deliverytimes = []
        if time.__len__() > 0:
            deliverytimes.append(int(time[0]))
        else:
            deliverytimes = config["deliverytimes"]

        for deliverytime in deliverytimes:
            if debug_flag: print "Processing station: " + str(station)
            if debug_flag: print "Processing time: " + str(deliverytime)

            if dontUseSaturdaySunday:
                processDateTime = datetime.datetime.combine(processingDate, miscFunctions.create_time(deliverytime))
                if datetime.datetime.today() > processDateTime:
                    outputline = outputline + synopcode.createSynopCode(processingDate, str(deliverytime), station, connection, tableNameString, debug_flag).strip() + "=\n" 
            else:
                processDateTime = datetime.datetime.combine(saturday, miscFunctions.create_time(deliverytime))
                if datetime.datetime.today() > processDateTime:
                    if debug_flag: print "Saturday: "
                    outputline = outputline + synopcode.createSynopCode(saturday, str(deliverytime), station, connection, tableNameString, debug_flag).strip() + "=\n"

                processDateTime = datetime.datetime.combine(saturday, miscFunctions.create_time(deliverytime))
                if datetime.datetime.today() > processDateTime:
                    if debug_flag: print "Sunday: "
                    outputline = outputline + synopcode.createSynopCode(sunday, str(deliverytime), station, connection, tableNameString, debug_flag).strip() + "=\n"
                if debug_flag: print "\n"
            # END LOOP for deliverytimes
        if debug_flag: print "\n"
        # END LOOP for stations

    connection.close()
    
    if processingDate != None and str(deliverytime).__len__() > 0:
        if not os.path.exists(opath):
                os.makedirs(opath) 
        
        file = opath + os.sep + "output.txt"
        if ofile.__len__() > 0:
            file = ofile
        fid = open(file, "w")
        fid.write("ZCZC\n")
        fid.write("SMOS01 LOWM " + processingDate.strftime("%d") + miscFunctions.add_zeros(str(deliverytime), 4)[0:2] + "00" + "\n") 
        fid.write(synopcode.create_leading_part(processingDate, str(deliverytime)))
        fid.write(outputline)
        fid.write("NNNN\n")
        #print outputline
        fid.close()
        