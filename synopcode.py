#!/usr/bin/env python
# -*- coding: utf-8 -*-

# synopcode.py
#
# purpose:  Wetterturnier.de
# author:   Szabolcs KÖLLÖ
# e-mail:   szabolcs.koelloe@uibk.ac.at
# web:      http://imgi.uibk.ac.at/
# created:  13-Aug-2013
# modified:

# Synopcode DOC:
# http://www.met.fu-berlin.de/~stefan/fm12.html

import dateFunctions
import miscFunctions

__author__ = 'szabo'

EMPTYSTRING = ""
DONTUSETHISVALUE = -998
WEATHERCODEDONTUSETHISVALUE = 254

TIME0000 = 0
TIME0100 = 100
TIME0600 = 600
TIME1200 = 1200
TIME1800 = 1800

SYNOP2table_name = "synop2_imgi"
TAWES2table_name = "tawes3_imgi"

NOPRECIPITATIONCODE = "990"
BASEPERIOD01H = "5"
BASEPERIOD06H = "1"
BASEPERIOD12H = "2"

WINDLEADINGNUMBER = "0"
TEMPLEADINGNUMBER = "1"
MAXTEMPLEADINGNUMBER = "1"
MINTEMPLEADINGNUMBER = "2"
DEWPOINTLEADINGNUMBER = "2"
ATMOSPHERICPRESSUERELEADINGNUMBER = "3"
PRESSURESEALEVELLEADINGNUMBER = "4"
PRECIPITATIONLEADINGNUMBER = "6"
WEATHERLEADINGNUMBER = "7"
GUSTLEADINGNUMBERLAST10MIN = "910"
GUSTLEADINGNUMBERLASTMESSAGE = "911"
GUSTLEADINGNUMBERLASTMESSAGEAVG = "912"

FIXLANDSTATION = "AAXX "
DETAILBLOCK333 = "333 "
DETAILBLOCK555 = "555 "

# ---------------------------------------------------------------------------
# create synopcode for sunshine duration
# ---------------------------------------------------------------------------
def create_sunshineduration_code(dbHandle, table_name,
                                 statnr, date, time, debugFlag):
    cursor = dbHandle.cursor()

    if table_name == SYNOP2table_name:
        query = \
            "SELECT sonne " \
            "FROM " + SYNOP2table_name + " " \
            "WHERE statnr = %s " \
            "AND datum = %s " \
            "AND stdmin = %s " \
            "AND sonne < %s"

        query = query % (statnr,
                               str(dateFunctions.correct_year(date)),
                               int(time),
                               WEATHERCODEDONTUSETHISVALUE)
        
        if debugFlag: print query
        cursor.execute(query)
        
        row = cursor.fetchone()
        if row:
            sumSun = row[0]
        else:
            return EMPTYSTRING

    else:
        if int(time) == TIME0100:
            query = \
                "SELECT sum(so) sumsonne " \
                "FROM " + TAWES2table_name + " " \
                "WHERE statnr = %s " \
                "AND Datum = %s " \
                "AND stdmin > %s " \
                "AND stdmin <= %s " \
                "UNION " \
                "SELECT sum(so) sumsonne " \
                "FROM " + TAWES2table_name + " " \
                "WHERE statnr = %s " \
                "AND Datum = %s " \
                "AND stdmin > %s " \
                "AND stdmin <= %s "
    
            query = query % (statnr,
                                   str(dateFunctions.get_one_day_before_date(date)).replace("-", ""),
                                   2350,#2300,
                                   2400,#2400,
                                   statnr,
                                   str(date).replace("-", ""),
                                   0,
                                   50)        
        else:
            query = \
                "SELECT sum(so) sumsonne " \
                "FROM " + TAWES2table_name + " " \
                "WHERE statnr = %s " \
                "AND Datum = %s " \
                "AND stdmin > %s " \
                "AND stdmin <= %s"
    
            if int(time) == TIME0000:
                query = query % (      
                                    statnr,
                                    str(dateFunctions.get_one_day_before_date(date)).replace("-", ""),
                                    2250,
                                    2350
                                )
            else:            
                query = query % (      
                                    statnr,
                                    str(date).replace("-", ""),
                                    int(time) - 150,
                                    int(time) - 50
                                )            
        if debugFlag: print query
        cursor.execute(query)
        
        row = cursor.fetchone()
        if row:
            sumSun = row[0]
        else:
            return EMPTYSTRING

        if sumSun == None:
            return EMPTYSTRING
        
        sumSun = int(float(sumSun)/360.0 + 0.5)

    return "553" + str(miscFunctions.add_zeros(sumSun, 2)) + " "

# ---------------------------------------------------------------------------
# create synopcode for sunshine duration
# ---------------------------------------------------------------------------
def create_sunshineduration_code_0600(dbHandle, table_name,
                                 statnr, date, time, debugFlag):
    
    if not int(time) == 600:
        return EMPTYSTRING 
    
    cursor = dbHandle.cursor()

    if table_name == SYNOP2table_name:
        query = \
            "SELECT sonnetag " \
            "FROM " + SYNOP2table_name + " " \
            "WHERE statnr = %s " \
            "AND datum = %s " \
            "AND stdmin = %s " \
            "AND sonne < %s"

        query = query % (statnr,
                               str(dateFunctions.correct_year(date)),
                               600,
                               WEATHERCODEDONTUSETHISVALUE)
        
        if debugFlag: print query
        cursor.execute(query)
        
        row = cursor.fetchone()
        if row:
            sumSun = row[0]
        else:
            return EMPTYSTRING

    else:
        query = \
            "SELECT sum(so) sumsonne " \
            "FROM " + TAWES2table_name + " " \
            "WHERE statnr = %s " \
            "AND Datum = %s"

        query = query % (      
                            statnr,
                            str(dateFunctions.get_one_day_before_date(date)).replace("-", "")
                        )
        if debugFlag: print query
        cursor.execute(query)
        
        row = cursor.fetchone()
        if row:
            sumSun = row[0]
        else:
            return EMPTYSTRING

        if sumSun == None:
            return EMPTYSTRING
        
        sumSun = int(float(sumSun)/360.0 + 0.5)

    return "55" + str(miscFunctions.add_zeros(sumSun, 3)) + " "

# ---------------------------------------------------------------------------
# create synopcode for weather
# ---------------------------------------------------------------------------
def create_weather_code(dbHandle, table_name, statnr, date, time, debugFlag):
    if table_name == SYNOP2table_name:
        maxW1 = 0
        maxW2 = 0

        cursor = dbHandle.cursor()
        
        query = \
        "SELECT W1 maxW1 " \
        "FROM " + SYNOP2table_name + " " \
        "WHERE statnr = %s " \
        "AND datum = %s " \
        "AND stdmin = %s " \
        "AND W1 <> %s"

        query = query % (statnr,
                               str(dateFunctions.correct_year(date)),
                               int(time),
                               WEATHERCODEDONTUSETHISVALUE)
        
        if debugFlag: print query
        cursor.execute(query)
        
        row = cursor.fetchone()
        if row:
            maxW1 = row[0]
        else:
            return EMPTYSTRING

        query = \
        "SELECT W2 maxW2 " \
        "FROM " + SYNOP2table_name + " " \
        "WHERE statnr = %s " \
        "AND datum = %s " \
        "AND stdmin = %s " \
        "AND W1 <> %s"

        query = query % (statnr,
                               str(dateFunctions.correct_year(date)),
                               int(time),
                               WEATHERCODEDONTUSETHISVALUE)
        
        if debugFlag: print query
        cursor.execute(query)
        
        row = cursor.fetchone()
        if row:
            maxW2 = row[0]
        else:
            return EMPTYSTRING
        
        query = \
        "SELECT ww " \
        "FROM " + SYNOP2table_name + " " \
        "WHERE statnr = %s " \
        "AND ww <> %s " \
        "AND datum = %s " \
        "AND stdmin = %s"

        query = query % (statnr,
                               254,
                               str(dateFunctions.correct_year(date)),
                               int(time))
        if debugFlag: print query
        cursor.execute(query)
        

        row = cursor.fetchone()
        if row:
            ww = row[0]
        else:
            return EMPTYSTRING

        if ww == None:
            return EMPTYSTRING

        if (maxW1 >= 10):
            return EMPTYSTRING

        if (maxW2 >= 10):
            return EMPTYSTRING

        return WEATHERLEADINGNUMBER + \
               miscFunctions.add_zeros(str(ww), 2) + \
               str(maxW1) + \
               str(maxW2) + \
               " "

    #TAWES2 table has no weathercode
    return EMPTYSTRING

# ---------------------------------------------------------------------------
# laut RETO wird das so geliefert :-)
# ---------------------------------------------------------------------------
def create_indicator_code(dbHandle, table_name, statnr, date, time, debugFlag, participation, weather):
    '''
    iR -- Indikator für Niederschlagsgruppen:
    0 -- Niederschlag wird in den Abschnitten 1 und 3 gemeldet
    1 -- Niederschlag wird nur in Abschnitt 1 gemeldet
    2 -- Niederschlag wird nur in Abschnitt 3 gemeldet
    3 -- Niederschlag nicht gemeldet -- kein Niederschlag vorhanden
    4 -- Niederschlag nicht gemeldet -- Niederschlagsmessung nicht durchgeführt oder nicht vorgesehen
    
    iX -- Indikator für den Stationstyp sowie für Wettergruppen:
    
    1 -- bemannte Station -- Wettergruppe wird gemeldet
    2 -- bemannte Station -- Wettergruppe nicht gemeldet -- kein signifikantes Wetter
    3 -- bemannte Station -- Wettergruppe nicht gemeldet -- Wetterbeobachtung nicht durchgeführt
    4 -- automatische Station, Typ 1 -- Wettergruppe gemeldet
    5 -- automatische Station, Typ 1 -- Wettergruppe nicht gemeldet -- kein signifikantes Wetter
    6 -- automatische Station, Typ 2 -- Wettergruppe nicht gemeldet -- Wetter nicht feststellbar
    7 -- automatische Station, Typ 2 -- Wettergruppe wird gemeldet
    '''
    
    hS = "/"
    VVS = "//"
    hVV = EMPTYSTRING
    IrIxS = EMPTYSTRING
    if table_name == SYNOP2table_name:
        cursor = dbHandle.cursor()
        query = \
                "SELECT Ir, Ix, h, VV " \
                "FROM " + SYNOP2table_name + " " \
                "WHERE statnr = %s " \
                "AND datum = %s " \
                "AND stdmin = %s"
    
        query = query % (statnr,
                               str(dateFunctions.correct_year(date)),
                               int(time)
                               )
        
        if debugFlag: print query
        cursor.execute(query)
                
        row = cursor.fetchone()
        if row:
            Ir, Ix, h, VV = row
            if (h < 10):
                hS = str(h)
                
            if (VV < 100):
                VVS = miscFunctions.add_zeros(VV, 2)
                
            hVV = hS + VVS
            IrIxS = str(Ir) + str(Ix)        
    
    pdigit = 3
    if participation:
        pdigit = 1
        
    wdigit = 6
    if weather:
        wdigit = 5
    
    if table_name == SYNOP2table_name:
        return IrIxS + hVV + " "
    
    #in tawes2 table are no informations for clouds and horizontal sight
    return str(pdigit) + str(wdigit) + "/// "

# ---------------------------------------------------------------------------
# create synopcode for precipitation
# ---------------------------------------------------------------------------
def precipitationCode(dbHandle, table_name, statnr, date, time, debugFlag):
    sum = 0
    cursor = dbHandle.cursor()
    BASEPERIOD = BASEPERIOD01H

    if table_name == SYNOP2table_name:
        participationcolumn_name = "RRR"
        
        query = "SELECT %s FROM %s WHERE statnr = %s AND datum = %s AND stdmin = %s AND %s <> %s"
    
        query = query % (participationcolumn_name, 
                         SYNOP2table_name,
                         statnr,
                         str(dateFunctions.correct_year(date)),
                         int(time),
                         participationcolumn_name,
                         DONTUSETHISVALUE
                         )
        
        if debugFlag: print query                       
        cursor.execute(query)        
        
        row = cursor.fetchone()
        if row:
            value = row[0]
        else:
            return EMPTYSTRING
    else:
        #Begin her for TAWES2
        participationcolumn_name = "rr"
        if int(time) == TIME0000:
            query = \
                "SELECT " + participationcolumn_name + " " \
                "FROM " + TAWES2table_name + " " \
                "WHERE statnr = %s " \
                "AND Datum = %s " \
                "AND stdmin > %s " \
                "AND stdmin <= %s " \
                "AND " + participationcolumn_name + " <> %s"
    
            query = query % (      statnr,
                                   str(dateFunctions.get_one_day_before_date(date)).replace("-", ""),
                                   TIME1800 - 50,
                                   TIME0000 - 50,
                                   DONTUSETHISVALUE)
            if debugFlag: 
                print query
            cursor.execute(query)
            BASEPERIOD = BASEPERIOD06H
            
        elif int(time) == TIME0600:
            query = \
            "SELECT " + participationcolumn_name + " " \
                "FROM " + TAWES2table_name + " " \
                "WHERE statnr = %s " \
                "AND Datum = %s " \
                "AND stdmin > %s " \
                "AND stdmin <= %s " \
                "AND " + participationcolumn_name + " <> %s " \
                "UNION " \
                "SELECT " + participationcolumn_name + " " \
                "FROM " + TAWES2table_name + " " \
                "WHERE statnr = %s " \
                "AND Datum = %s " \
                "AND stdmin > %s " \
                "AND stdmin <= %s " \
                "AND " + participationcolumn_name + " <> %s "
    
            query = query % (      statnr,
                                   str(dateFunctions.get_one_day_before_date(date)).replace("-", ""),
                                   1800,#2300,
                                   2400,#2400,
                                   DONTUSETHISVALUE,
                                   statnr,
                                   str(date).replace("-", ""),
                                   TIME0000,
                                   TIME0600 - 50,
                                   DONTUSETHISVALUE)
            if debugFlag: 
                print query
            cursor.execute(query)
            BASEPERIOD = BASEPERIOD12H 
        
        elif int(time) == TIME1200:
            query = \
                "SELECT " + participationcolumn_name + " " \
                "FROM " + TAWES2table_name + " " \
                "WHERE statnr = %s " \
                "AND Datum = %s " \
                "AND stdmin > %s " \
                "AND stdmin <= %s " \
                "AND " + participationcolumn_name + " <> %s"
    
            query = query % (      statnr,
                                   str(date).replace("-", ""),
                                   TIME0600 - 50,
                                   TIME1200 - 50,
                                   DONTUSETHISVALUE)
            if debugFlag: 
                print query
            cursor.execute(query)
            BASEPERIOD = BASEPERIOD06H
            
        elif int(time) == TIME1800:
            query = \
                "SELECT " + participationcolumn_name + " " \
                "FROM " + TAWES2table_name + " " \
                "WHERE statnr = %s " \
                "AND Datum = %s " \
                "AND stdmin > %s " \
                "AND stdmin <= %s " \
                "AND " + participationcolumn_name + " <> %s"
    
            query = query % (      statnr,
                                   str(date).replace("-", ""),
                                   TIME0600 - 50,
                                   TIME1800 - 50,
                                   DONTUSETHISVALUE)
            if debugFlag: 
                print query
            cursor.execute(query)
            BASEPERIOD = BASEPERIOD12H
               
        elif int(time) == TIME0100:
            query = \
                "SELECT " + participationcolumn_name + " " \
                "FROM " + TAWES2table_name + " " \
                "WHERE statnr = %s " \
                "AND Datum = %s " \
                "AND stdmin > %s " \
                "AND stdmin <= %s " \
                "AND " + participationcolumn_name + " <> %s " \
                "UNION " \
                "SELECT " + participationcolumn_name + " " \
                "FROM " + TAWES2table_name + " " \
                "WHERE statnr = %s " \
                "AND Datum = %s " \
                "AND stdmin > %s " \
                "AND stdmin <= %s " \
                "AND " + participationcolumn_name + " <> %s "
    
            query = query % (statnr,
                                   str(dateFunctions.get_one_day_before_date(date)).replace("-", ""),
                                   2350,#2300,
                                   2400,#2400,
                                   DONTUSETHISVALUE,
                                   statnr,
                                   str(date).replace("-", ""),
                                   0,
                                   50,
                                   DONTUSETHISVALUE)
            if debugFlag: print query
            cursor.execute(query)
            BASEPERIOD = BASEPERIOD01H
        else:
            query = \
                "SELECT " + participationcolumn_name + " " \
                "FROM " + TAWES2table_name + " " \
                "WHERE statnr = %s " \
                "AND Datum = %s " \
                "AND stdmin > %s " \
                "AND stdmin <= %s " \
                "AND " + participationcolumn_name + " <> %s"
    
            query = query % (statnr,
                                   str(date).replace("-", ""),
                                   int(time) - 150,
                                   int(time) - 50,
                                   DONTUSETHISVALUE)
            
            if debugFlag: print query
            cursor.execute(query)            
    
        rows = cursor.fetchall()
    
        for row in rows:
            sum += float(row[0]) #sum = sum + float(row[0])
            
        value = sum
        #END here for TAWES2
        
    if (value >= 10):
        value = int(value/10)
    elif value > 0:
        value = int(NOPRECIPITATIONCODE) + int(value)
    elif (value == 0):
        value = NOPRECIPITATIONCODE
    else:
        return EMPTYSTRING

    value = miscFunctions.add_zeros(value, 3)

    return PRECIPITATIONLEADINGNUMBER + str(value) + BASEPERIOD + " "

# ---------------------------------------------------------------------------
# generic pressureCode
# value in 1/10 Hectopascal 4 or 5 digits
# if given value bigger 10000 value will be flipped
# if value 5 digits long, it will be shortened to 4 digits
# if value shorter then 4 digits, leading zeros will be added.
# ---------------------------------------------------------------------------
def pressureCode(value):
    if not miscFunctions.check_value_empty(value):
        return EMPTYSTRING

    if not miscFunctions.check_value_to_long(value, 5):
        return EMPTYSTRING

    if not miscFunctions.check_value_to_short(value, 4):
        return EMPTYSTRING

    value = miscFunctions.flip_thousand(value, 10000)

    if len(value) == 5:
        value = str(round(float(value)/10.0, 0))

    return miscFunctions.add_zeros(value, 4)

# ---------------------------------------------------------------------------
# create synopcode for atmospheric pressure sea level
# in synop2 column: Pp -> corrected to sea level
# in tawes2 column: pred -> corrected to sea level
# ---------------------------------------------------------------------------
def create_sealevelpressure_code(dbHandle, table_name, statnr, date, time, debugFlag):
    Pp = EMPTYSTRING
    cursor = dbHandle.cursor()

    if table_name == SYNOP2table_name:
        query = "SELECT Pp " \
            "FROM " + SYNOP2table_name + " " + \
            "WHERE statnr = %s " \
            "AND datum = %s " \
            "AND stdmin = %s"
    elif table_name == TAWES2table_name:
        query = "SELECT pred " \
            "FROM " + TAWES2table_name + " " + \
            "WHERE statnr = %s " \
            "AND Datum = %s " \
            "AND stdmin = %s"
    else:
       return EMPTYSTRING

    if int(time) == TIME0000 and table_name == TAWES2table_name:
        query = query % (      statnr,
                               str(dateFunctions.get_one_day_before_date(date)).replace("-", ""),
                               2350)
        if debugFlag: print query
        cursor.execute(query)
    elif table_name == TAWES2table_name:
        query = query % (statnr,
                               str(date).replace("-", ""),
                               int(time) - 50)
        if debugFlag: print query     
        cursor.execute(query)
    else:
        query = query % (statnr,
                               str(dateFunctions.correct_year(date)),
                               int(time))
        if debugFlag: print query
        cursor.execute(query)
    
    row = cursor.fetchone()
    if row:
        Pp = row[0]
    else:
        return EMPTYSTRING

    return PRESSURESEALEVELLEADINGNUMBER + pressureCode(str(Pp)) + " "

# ---------------------------------------------------------------------------
# create synopcode for atmospheric pressure
# in synop2 column: Pg -> Rawdata
# in tawes2 column: p -> Rawdata
# ---------------------------------------------------------------------------
def create_atmosphericpressure_code(dbHandle, table_name, statnr, date, time, debugFlag):
    Pg = EMPTYSTRING
    cursor = dbHandle.cursor()

    if table_name == SYNOP2table_name:
        query = "SELECT Pg " \
            "FROM " + SYNOP2table_name + " " + \
            "WHERE statnr = %s " \
            "AND datum = %s " \
            "AND stdmin = %s"
    elif table_name == TAWES2table_name:
        query = "SELECT p " \
            "FROM " + TAWES2table_name + " " + \
            "WHERE statnr = %s " \
            "AND Datum = %s " \
            "AND stdmin = %s"
    else:
       return EMPTYSTRING

    if int(time) == TIME0000 and table_name == TAWES2table_name:
        query = query % (statnr,
                               str(dateFunctions.get_one_day_before_date(date)).replace("-", ""),
                               2350)
        if debugFlag: print query
        cursor.execute(query)   
    elif table_name == TAWES2table_name:
        query = query % (statnr,
                               str(date).replace("-", ""),
                               int(time) - 50)
        if debugFlag: print query
        cursor.execute(query)
    else:
        query = query % (statnr,
                               str(dateFunctions.correct_year(date)),
                               int(time))
        if debugFlag: print query
        cursor.execute(query)
    
    row = cursor.fetchone()
    if row :
        Pg = row[0]
    else:
        return EMPTYSTRING

    return ATMOSPHERICPRESSUERELEADINGNUMBER + pressureCode(str(Pg)) + " "

# ---------------------------------------------------------------------------
# create synopcode for dewpoint
# ---------------------------------------------------------------------------
def create_dewpoint_code(dbHandle, table_name, statnr, date, time, debugFlag):
    Td = EMPTYSTRING
    cursor = dbHandle.cursor()

    if table_name == SYNOP2table_name:
        query = "SELECT Td " \
            "FROM " + SYNOP2table_name + " " + \
            "WHERE statnr = %s " \
            "AND datum = %s " \
            "AND stdmin = %s"
    elif table_name == TAWES2table_name:
        query = "SELECT tp " \
            "FROM " + TAWES2table_name + " " + \
            "WHERE statnr = %s " \
            "AND Datum = %s " \
            "AND stdmin = %s"
    else:
       return EMPTYSTRING
    
    if int(time) == TIME0000 and table_name == TAWES2table_name:
        query = query % (statnr,
                               str(dateFunctions.get_one_day_before_date(date)).replace("-", ""),
                               2350)
    elif table_name == TAWES2table_name:
        query = query % (statnr,
                               str(date).replace("-", ""),
                               int(time) - 50)
    else:
        query = query % (statnr,
                               str(dateFunctions.correct_year(date)),
                               int(time))
    
    if debugFlag: print query
    cursor.execute(query)
    
    row = cursor.fetchone()
    if row :
        Td = row[0]

    if not miscFunctions.check_value_empty(Td):
        return EMPTYSTRING

    if not miscFunctions.check_value_to_long(Td, 3):
        return EMPTYSTRING

    return DEWPOINTLEADINGNUMBER + \
           miscFunctions.create_sign(Td, table_name, "tp") + \
           miscFunctions.add_zeros(miscFunctions.invert_value(Td), 3) \
           + " "

# ---------------------------------------------------------------------------
# create synopcode for temperature
# synop2 table values are ready, in tawes2 we have to search for min, max value
# ---------------------------------------------------------------------------
def create_temp_code(dbHandle, table_name, statnr, date, time, debugFlag):
    temperature = EMPTYSTRING
    cursor = dbHandle.cursor()

    if table_name == SYNOP2table_name:
        column_name = "T"
    elif table_name == TAWES2table_name:
        column_name = "tl"
    else:
       return EMPTYSTRING

    query = "SELECT " + column_name + " " \
            "FROM " + table_name + " " \
            "WHERE statnr = %s " \
            "AND %s = %s " \
            "AND stdmin = %s"

    if int(time) == TIME0000 and table_name == TAWES2table_name:
        query = query % (      statnr,
                               "Datum", str(dateFunctions.get_one_day_before_date(date)).replace("-", ""),
                               2350)  
    elif table_name == TAWES2table_name:
        query = query % (      statnr,
                               "Datum", str(date).replace("-", ""),
                               int(time) - 50)
    else:
        query = query % (      statnr,
                               "datum", str(dateFunctions.correct_year(date)),
                               int(time))
        
    cursor.execute(query)
    row = cursor.fetchone()

    if row:
        temperature = row[0]
    else:
        return EMPTYSTRING

    return TEMPLEADINGNUMBER + \
           miscFunctions.create_sign(temperature) + \
           miscFunctions.add_zeros(miscFunctions.invert_value(temperature), 3) \
           + " "

# ---------------------------------------------------------------------------
# create synopcode for max temperature
# synop2 table values are ready, in tawes2 we have to search for min, max value
# ---------------------------------------------------------------------------
def create_max_temp_code(dbHandle, table_name, statnr, date, time, debugFlag):
    temperature = EMPTYSTRING
    cursor = dbHandle.cursor()

    if table_name == SYNOP2table_name:
        if int(time) == TIME1800 or int(time) == TIME0600:
            query = "SELECT Tmax " \
                    "FROM " + SYNOP2table_name + " " \
                    "WHERE statnr = %s " \
                    "AND datum = %s " \
                    "AND stdmin = %s"
        else:
            return EMPTYSTRING

        query = query % (statnr,
                               str(dateFunctions.correct_year(date)),
                               int(time))
        if debugFlag: print query
        cursor.execute(query)
        

    elif table_name == TAWES2table_name:
        if int(time) == TIME1800:
            query = "SELECT max(tlmax) [max] " \
                    "FROM " + TAWES2table_name + " " \
                    "WHERE statnr = %s " \
                    "AND Datum = %s " \
                    "AND stdmin > %s " \
                    "AND stdmin <= %s"
            query = query % (statnr,
                                   str(date).replace("-", ""),
                                   TIME0600 - 50,
                                   TIME1800 - 50)
            
            if debugFlag: print query
            cursor.execute(query)
            
        elif int(time) == TIME0600:
            query = "SELECT TOP 1 tlmax " \
                    "FROM " + TAWES2table_name + " " \
                    "WHERE statnr = %s " \
                    "AND Datum = %s " \
                    "AND stdmin > %s " \
                    "UNION " \
                    "SELECT tlmax " \
                    "FROM " + TAWES2table_name + " " \
                    "WHERE statnr = %s " \
                    "AND Datum = %s " \
                    "AND stdmin <= %s " \
                    "ORDER BY tlmax DESC"
            query = query % (statnr,
                                   str(dateFunctions.get_one_day_before_date(date)).replace("-", ""),
                                   TIME1800 - 50,
                                   statnr,
                                   str(date).replace("-", ""),
                                   TIME0600 -50)
            
            if debugFlag: print query
            cursor.execute(query)
            
        else:
            return EMPTYSTRING

    else:
       return EMPTYSTRING

    row = cursor.fetchone()

    if row:
        temperature = row[0]
    else:
        return EMPTYSTRING
    
    if temperature == None:
        return EMPTYSTRING
    
    if int(temperature) <= DONTUSETHISVALUE:
        return EMPTYSTRING

    return MAXTEMPLEADINGNUMBER + \
           miscFunctions.create_sign(temperature) + \
           miscFunctions.add_zeros(miscFunctions.invert_value(temperature), 3) + \
           " "

# ---------------------------------------------------------------------------
# create synopcode for min temperature
# synop2 table values are ready, in tawes2 we have to search for min, max value
# ---------------------------------------------------------------------------
def create_min_temp_code(dbHandle, table_name, statnr, date, time, debugFlag):
    temperature = EMPTYSTRING
    cursor = dbHandle.cursor()

    if table_name == SYNOP2table_name:
        if int(time) == TIME1800 or int(time) == TIME0600:
            query = "SELECT Tmin " \
                    "FROM " + SYNOP2table_name + " " \
                    "WHERE statnr = %s " \
                    "AND datum = %s " \
                    "AND Tmin > %s " \
                    "AND stdmin = %s"
        else:
            return EMPTYSTRING

        query = query % (statnr,
                               str(dateFunctions.correct_year(date)),
                               DONTUSETHISVALUE,
                               int(time))
        
        if debugFlag: print query
        cursor.execute(query)
        

    elif table_name == TAWES2table_name:
        if int(time) == TIME1800:
            query = "SELECT min(tlmin) [min] " \
                    "FROM " + TAWES2table_name + " " \
                    "WHERE statnr = %s " \
                    "AND Datum = %s " \
                    "AND tlmin > %s " \
                    "AND stdmin > %s " \
                    "AND stdmin <= %s"
            query = query % (statnr,
                                   str(date).replace("-", ""),
                                   DONTUSETHISVALUE,
                                   TIME0600 - 50,
                                   TIME1800 - 50)
            if debugFlag: print query
            cursor.execute(query)
            
        elif int(time) == TIME0600:
            query = "SELECT TOP 1 tlmin " \
                    "FROM " + TAWES2table_name + " " \
                    "WHERE statnr = %s " \
                    "AND Datum = %s " \
                    "AND tlmin > %s " \
                    "AND stdmin > %s " \
                    "UNION " \
                    "SELECT tlmin " \
                    "FROM " + TAWES2table_name + " " \
                    "WHERE statnr = %s " \
                    "AND Datum = %s " \
                    "AND tlmin > %s " \
                    "AND stdmin <= %s " \
                    "ORDER BY tlmin ASC"
            query = query % (      statnr,
                                   str(dateFunctions.get_one_day_before_date(date)).replace("-", ""),
                                   DONTUSETHISVALUE,
                                   TIME1800 - 50,
                                   statnr,
                                   str(date).replace("-", ""),
                                   DONTUSETHISVALUE,
                                   TIME0600 - 50)
            if debugFlag: print query
            cursor.execute(query)
            
        else:
            return EMPTYSTRING

    else:
       return EMPTYSTRING

    row = cursor.fetchone()

    if row:
        temperature = row[0]
    else:
        return EMPTYSTRING
    
    if temperature == None:
        return EMPTYSTRING
    
    return MINTEMPLEADINGNUMBER + \
           miscFunctions.create_sign(temperature) + \
           miscFunctions.add_zeros(miscFunctions.invert_value(temperature), 3) + \
           " "

# ---------------------------------------------------------------------------
# create synopcode for wind
# ---------------------------------------------------------------------------
def createWindCode(dbHandle, table_name, statnr, date, time, debugFlag):
    ff = EMPTYSTRING
    cursor = dbHandle.cursor()

    query = "SELECT ff " \
            "FROM " + table_name + " " + \
            "WHERE statnr = %s " \
            "AND datum = %s " \
            "AND stdmin = %s"

    query = query % (statnr,
                           str(dateFunctions.correct_year(date)),
                           int(time))
    
    if debugFlag: print query
    cursor.execute(query)
    
    row = cursor.fetchone()
    if row :
        ff = row[0]

    if not miscFunctions.check_value_empty(ff):
        return EMPTYSTRING

    if not miscFunctions.check_value_to_long(ff, 3):
        return EMPTYSTRING

    return WINDLEADINGNUMBER + "0" + miscFunctions.add_zeros(ff, 3) + " "

# ---------------------------------------------------------------------------
# create synopcode for cloudscoverlevel, winddirection, windspeed
# in TAWES2 cloudscoverlevel not observed
# ---------------------------------------------------------------------------
def create_covering_winddirection_speed_code(dbHandle, table_name, statnr, date, time, debugFlag):
    N = EMPTYSTRING
    dd = EMPTYSTRING
    ff = EMPTYSTRING
    OOfff = EMPTYSTRING

    cursor = dbHandle.cursor()

    query = "SELECT %s, dd, ff " \
            "FROM " + table_name + " " + \
            "WHERE statnr = %s " \
            "AND %s = %s " \
            "AND stdmin = %s"
    
    if int(time) == TIME0000 and table_name == TAWES2table_name:
        query = query % (   "ffn_2",
                            statnr,
                            "Datum", str(dateFunctions.get_one_day_before_date(date)).replace("-", ""),
                            2350)
        
    elif table_name == TAWES2table_name:
        query = query % (   "ffn_2", 
                            statnr,
                            "Datum", str(date).replace("-", ""),
                            int(time) - 50)
    else:
        query = query % (   "N",
                            statnr,
                            "datum", str(dateFunctions.correct_year(date)),
                            int(time))

    if debugFlag: print query
    cursor.execute(query)
        
    row = cursor.fetchone()
    
    if row :
        N, dd, ff = row

    if not miscFunctions.check_value_empty(N):
        return EMPTYSTRING

    if not miscFunctions.check_value_empty(dd):
        return EMPTYSTRING

    if not miscFunctions.check_value_empty(ff):
        return EMPTYSTRING

    ff = int(round(float(ff)/10.0, 0))

    if int(ff) > 99:
        ff = "//" # no definition in SYNOPcode, RETO say // :-)
        OOfff = createWindCode(dbHandle, table_name, statnr, date, time, debugFlag) + " "

    if int(N) > 9:
        N = "/"

    if table_name == TAWES2table_name:
        N = "/"

    dd = str(int(dd/10.0 + 0.5))

    return str(N) + \
           miscFunctions.add_zeros(dd, 2) + \
           miscFunctions.add_zeros(ff, 2) + \
           " " + \
           OOfff

# ---------------------------------------------------------------------------
# create synopcode for gust last 10 min
# ---------------------------------------------------------------------------
def create_gust_code_last_10_min(dbHandle, table_name, statnr, date, time, debugFlag):
    boe = EMPTYSTRING
    cursor = dbHandle.cursor()

    if table_name == SYNOP2table_name:
        column_name = "boe"
        
        query = "SELECT " + column_name + " " \
                "FROM " + table_name + " " + \
                "WHERE statnr = %s " \
                "AND datum = %s " \
                "AND " + column_name + " <> %s " \
                "AND stdmin = %s"
                
        query = query % (statnr,
                               str(dateFunctions.correct_year(date)),
                               DONTUSETHISVALUE,
                               int(time))   
        
        if debugFlag: print query
        cursor.execute(query)
        
    elif table_name == TAWES2table_name:
        column_name = "ffx"
        
        query = "SELECT " + column_name + " " \
                "FROM " + table_name + " " + \
                "WHERE statnr = %s " \
                "AND Datum = %s " \
                "AND " + column_name + " <> %s " \
                "AND stdmin = %s"
        
        if int(time) == TIME0000:
                query = query % (statnr,
                               str(dateFunctions.get_one_day_before_date(date)).replace("-", ""),
                               DONTUSETHISVALUE,
                               2350)
                
                if debugFlag: print query
                cursor.execute(query)
        else:
                query = query % (statnr,
                               str(date).replace("-", ""),
                               DONTUSETHISVALUE,
                               int(time) - 50)
                
                if debugFlag: print query
                cursor.execute(query)
        
    else:
       return EMPTYSTRING
    
    row = cursor.fetchone()

    if row :
        boe = row[0]
    else:
        return EMPTYSTRING

    if not miscFunctions.check_value_empty(boe):
        return EMPTYSTRING

    if boe is None:
        return EMPTYSTRING

    #if int(boe) < 128: # 25 knots -> 12.86 m/s, data is in 1/10 knots in the table
    #    return EMPTYSTRING

    boe = int(boe/10.0 + 0.5)

    return GUSTLEADINGNUMBERLAST10MIN + \
           miscFunctions.add_zeros(str(boe), 2) + \
           " "

# ---------------------------------------------------------------------------
# create synopcode for gust last 10 min
# ---------------------------------------------------------------------------
def create_gust_code_last_message(dbHandle, table_name, statnr, date, time, debugFlag):
    boe = EMPTYSTRING
    cursor = dbHandle.cursor()

    if table_name == SYNOP2table_name:
        column_name = "w1boe"
        
        query = "SELECT " + column_name + " " \
                "FROM " + table_name + " " + \
                "WHERE statnr = %s " \
                "AND datum = %s " \
                "AND " + column_name + " <> %s " \
                "AND stdmin = %s"
                
        query = query % (statnr,
                               str(dateFunctions.correct_year(date)),
                               DONTUSETHISVALUE,
                               int(time))       
        
        cursor.execute(query) 
        
    elif table_name == TAWES2table_name:
        column_name = "ffx"
        
        query = "SELECT max(" + column_name + ") " \
                "FROM " + table_name + " " + \
                "WHERE statnr = %s " \
                "AND Datum = %s " \
                "AND " + column_name + " <> %s " \
                "AND stdmin > %s " \
                "AND stdmin <= %s"
        
        if int(time) == TIME0000:
                query = query % (statnr,
                               str(dateFunctions.get_one_day_before_date(date)).replace("-", ""),
                               DONTUSETHISVALUE,
                               2250,
                               2350)
                if debugFlag: print query
                cursor.execute(query) 
        else:
                query = query % (statnr,
                               str(date).replace("-", ""),
                               DONTUSETHISVALUE,
                               int(time) - 150,
                               int(time) - 50)
                if debugFlag: print query
                cursor.execute(query) 
    else:
       return EMPTYSTRING
    
    row = cursor.fetchone()

    if row :
        boe = row[0]
    else:
        return EMPTYSTRING

    if not miscFunctions.check_value_empty(boe):
        return EMPTYSTRING

    if boe is None:
        return EMPTYSTRING

    #if int(boe) < 128: # 25 knots -> 12.86 m/s, data is in 1/10 knots in the table
    #    return EMPTYSTRING
    
    # lassen wir drinnen ohne if boe > 12,5m Entscheidung vom 11.06.2014 by Reto

    boe = int(boe/10.0 + 0.5)
    
    return GUSTLEADINGNUMBERLASTMESSAGE + \
           miscFunctions.add_zeros(str(boe), 2) + \
           " "


# ---------------------------------------------------------------------------
# create synopcode for gust last 10 min
# ---------------------------------------------------------------------------
def create_gust_code_last_message_avg(dbHandle, table_name, statnr, date, time, debugFlag):
    boe = EMPTYSTRING
    cursor = dbHandle.cursor()

    if table_name == SYNOP2table_name:
        column_name = "w1ffmax"
        
        query = "SELECT " + column_name + " " \
                "FROM " + table_name + " " + \
                "WHERE statnr = %s " \
                "AND datum = %s " \
                "AND " + column_name + " <> %s " \
                "AND stdmin = %s"
                
        query = query % (statnr,
                               str(dateFunctions.correct_year(date)),
                               DONTUSETHISVALUE,
                               int(time))
        if debugFlag: print query
        cursor.execute(query)      
        
    elif table_name == TAWES2table_name:
        column_name = "ff"
        
        query = "SELECT max(" + column_name + ") " \
                "FROM " + table_name + " " + \
                "WHERE statnr = %s " \
                "AND Datum = %s " \
                "AND " + column_name + " <> %s " \
                "AND stdmin > %s " \
                "AND stdmin <= %s"
        
        if int(time) == TIME0000:
                query = query % (statnr,
                               str(dateFunctions.get_one_day_before_date(date)).replace("-", ""),
                               DONTUSETHISVALUE,
                               2250,
                               2350)
                if debugFlag: print query
                cursor.execute(query)
        else:
                query = query % (statnr,
                               str(date).replace("-", ""),
                               DONTUSETHISVALUE,
                               int(time) - 150,
                               int(time) - 50)
                if debugFlag: print query
                cursor.execute(query)
    else:
       return EMPTYSTRING
    
    

    row = cursor.fetchone()

    if row :
        boe = row[0]
    else:
        return EMPTYSTRING

    if not miscFunctions.check_value_empty(boe):
        return EMPTYSTRING

    if boe is None:
        return EMPTYSTRING

    #Reto no limition
    #if int(boe) < 128: # 25 knots -> 12.86 m/s, data is in 1/10 knots in the table
    #    return EMPTYSTRING
    
    # lassen wir drinnen ohne if boe > 12,5m Entscheidung vom 11.06.2014 by Reto

    boe = int(boe/10.0 + 0.5)

    return GUSTLEADINGNUMBERLASTMESSAGEAVG + \
           miscFunctions.add_zeros(str(boe), 2) + \
           " "

'''
8NsChshs -- Angaben zu den Wolkenschichten (Gruppe kann bis zu 4-mal verschlüsselt werden)
Ns -- Bedeckungsgrad der Wolkenschicht in Achteln
C -- Wolkengattung:
0 -- Cirrus (Ci)
1 -- Cirrocumulus (Cc)
2 -- Cirrostratus (Cs)
3 -- Altocumulus (Ac)
4 -- Altostratus (As)
5 -- Nimbostratus (Ns)
6 -- Stratocumulus (Sc)
7 -- Stratus (St)
8 -- Cumulus (Cu)
9 -- Cumulonimbus (Cb)
/ -- Wolkengattung nicht erkennbar

hs hs -- Höhe der Wolkenuntergrenze:
00 -- < 30 m (< 100 ft)
01 -- 30 m (100 ft)
02 -- 60 m (200 ft)
03 -- 90 m (300 ft)
...
50 -- 1500 m (5000 ft)
======================
56 -- 1800 m (6000 ft)
57 -- 2100 m (7000 ft)
...
80 -- 9000 m (30000 ft)
=======================
81 -- 10500 m (35000 ft)
82 -- 12000 m (40000 ft)
...
88 -- 21000 m (70000 ft)
89 -- höher als 21000 m (> 70000 ft)
====================================
90 -- 0 bis 49 m (0 - 166 ft)
91 -- 50 bis 99 m (167 - 333 ft)
92 -- 100 bis 199 m (334 - 666 ft)
93 -- 200 bis 299 m (667 - 999 ft)
94 -- 300 bis 599 m (1000 - 1999 ft)
95 -- 600 bis 999 m (2000 - 3333 ft)
96 -- 1000 bis 1499 m (3334 - 4999 ft)
97 -- 1500 bis 1999 m (5000 - 6666 ft)
98 -- 2000 bis 2499 m (6667 - 8333 ft)
99 -- 2500 m oder höher (> 8334 ft)
'''
def create_cloud_lvl(dbHandle, table_name, statnr, date, time, debugFlag, number):
    cursor = dbHandle.cursor()
    
    #column_sql = EMPTYSTRING
    column_sql = "Ns" + str(number) + " N, C" + str(number) + " C, hs" + str(number) + " h"
    '''
    if number == 1:
        column_sql = "Ns1 N, C1 C, hs1 h"
    elif number == 2:
        column_sql = "Ns2 N, C2 C, hs2 h"
    elif number == 3:
        column_sql = "Ns3 N, C3 C, hs3 h"
    elif number == 4:
        column_sql = "Ns4 N, C4 C, hs4 h"
    else:
        print "wrong number for create_cloud_lvl() was given!!!!!"
    '''    
    if table_name == SYNOP2table_name:
        query = "SELECT " + column_sql + " " \
                "FROM " + table_name + " " + \
                "WHERE statnr = %s " \
                "AND datum = %s " \
                "AND N <> %s " \
                "AND stdmin = %s"
        
        query = query % (statnr,
                               str(dateFunctions.correct_year(date)),
                               WEATHERCODEDONTUSETHISVALUE,
                               int(time))
        
        if debugFlag: print query
        cursor.execute(query)
        
        row = cursor.fetchone()    
        if row :
            N, C, h = row
            
            if int(h) > 99:
                return EMPTYSTRING
            
            if (int(C) < WEATHERCODEDONTUSETHISVALUE):
                C_String = str(C)
            else:
                C_String = "/"                
            
            return "8" + str(N) + str(C_String) + str(miscFunctions.add_zeros(h,2)) + " "        
        return EMPTYSTRING    
    return EMPTYSTRING

def create_cloud_lvl_basic(dbHandle, table_name, statnr, date, time, debugFlag):
    cursor = dbHandle.cursor()
    
    if table_name == SYNOP2table_name:
        query = "SELECT Nh, CL, CM, CH " \
                "FROM " + table_name + " " + \
                "WHERE statnr = %s " \
                "AND datum = %s " \
                "AND stdmin = %s"
                
        query = query % (statnr,
                               str(dateFunctions.correct_year(date)),
                               int(time))
        
        if debugFlag: print query
        cursor.execute(query)
        
        row = cursor.fetchone()    
        if row:
            Nh, Cl, Cm, Ch = row
            
            if int(Nh) > 9:
                return EMPTYSTRING
            
            if int(Cl) > 9:
                Cl = "/"
                
            if int(Cm) > 9:
                Cm = "/"
                
            if int(Ch) > 9:
                Ch = "/"
            
            return "8" + str(Nh) + str(Cl) + str(Cm) + str(Ch) + " "        
        return EMPTYSTRING    
    return EMPTYSTRING

def create_leading_part(date, time):
    #return date.strftime("%Y%m%d") + miscFunctions.add_zeros(time, 4) + \
    return FIXLANDSTATION + \
        date.strftime("%d") + miscFunctions.add_zeros(time, 4)[0:2] + "1 " + "\n"

'''
5appp -- Tendenz des Stationsluftdrucks über die letzten 3 Stunden
a -- Art der Drucktendenz:
0 -- erst steigend, dann fallend -- resultierender Druck gleich oder höher als zuvor
1 -- erst steigend, dann gleichbleibend -- resultierender Druck höher als zuvor
2 -- konstant steigend -- resultierender Druck höher als zuvor
3 -- erst fallend oder gleichbleibend, dann steigend -- resultierender Druck höher als zuvor
4 -- gleichbleibend -- resultierender Druck unverändert
5 -- erst fallend, dann steigend -- resultierender Druck gleich oder tiefer als zuvor
6 -- erst fallend, dann gleichbleibend -- resultierender Druck tiefer als zuvor
7 -- konstant fallend -- resultierender Druck tiefer als zuvor
8 -- erst steigend oder gleichbleibend, dann fallend -- resultierender Druck tiefer als zuvor

ppp -- Betrag der 3-stündigen Druckänderung in 1/10 Hektopascal
'''
def create_tendencypressure_code(dbHandle, table_name, statnr, date, time, debugFlag):
    cursor = dbHandle.cursor()
    if table_name == SYNOP2table_name:
        query = "SELECT a, p FROM %s WHERE statnr = %s AND datum = %s AND stdmin = %s"        
        query = query % (table_name, statnr, str(dateFunctions.correct_year(date)), int(time))        
        if debugFlag: print query
        cursor.execute(query)
        
        row = cursor.fetchone()
        aS = EMPTYSTRING
        pS = EMPTYSTRING
        if row :
            a, p = row
            
            if int(a) < 9:
                aS = str(a)
            
            if (int(p) < 1000):
                pS = miscFunctions.add_zeros(p, 3)                            
            
            if len(aS) > 0 and len(pS) > 0:
                return "5" + aS + pS + " "
        
        return EMPTYSTRING
       
    return EMPTYSTRING    

def createSynopCode(date, time, statnr, db_handle, table_name, debug_flag):
    synopstring333 = EMPTYSTRING
    synopstring555 = EMPTYSTRING

    b_IIIII = str(statnr) + " "
    b_Nddff = create_covering_winddirection_speed_code(db_handle, table_name, statnr, date, time, debug_flag)
    b_1sTTT = create_temp_code(db_handle, table_name, statnr, date, time, debug_flag)
    b_2sTTT = create_dewpoint_code(db_handle, table_name, statnr, date, time, debug_flag)
    b_3PPPP = create_atmosphericpressure_code(db_handle, table_name, statnr, date, time, debug_flag)
    b_4PPPP = create_sealevelpressure_code(db_handle, table_name, statnr, date, time, debug_flag)
    b_5PPPP = create_tendencypressure_code(db_handle, table_name, statnr, date, time, debug_flag)
    b_6RRRt = precipitationCode(db_handle, table_name, statnr, date, time, debug_flag)
    b_7wwWW = create_weather_code(db_handle, table_name, statnr, date, time, debug_flag)
    b_8NChh = create_cloud_lvl_basic(db_handle, table_name, statnr, date, time, debug_flag)
    b_333_55SSS = create_sunshineduration_code_0600(db_handle, table_name, statnr, date, time, debug_flag)
    b_333_1xTTT = create_max_temp_code(db_handle, table_name, statnr, date, time, debug_flag)
    b_333_2xTTT = create_min_temp_code(db_handle, table_name, statnr, date, time, debug_flag)
    b_333_553SS = create_sunshineduration_code(db_handle, table_name, statnr, date, time, debug_flag)
    b_333_8NChh1 = create_cloud_lvl(db_handle, table_name, statnr, date, time, debug_flag, 1)
    b_333_8NChh2 = create_cloud_lvl(db_handle, table_name, statnr, date, time, debug_flag, 2)
    b_333_8NChh3 = create_cloud_lvl(db_handle, table_name, statnr, date, time, debug_flag, 3)
    b_333_8NChh4 = create_cloud_lvl(db_handle, table_name, statnr, date, time, debug_flag, 4)
    b_333_910XX = create_gust_code_last_10_min(db_handle, table_name, statnr, date, time, debug_flag)
    b_333_911XX = create_gust_code_last_message(db_handle, table_name, statnr, date, time, debug_flag)
    b_333_912XX = create_gust_code_last_message_avg(db_handle, table_name, statnr, date, time, debug_flag)

    partitionflag = False
    weatherflag = False
    
    if (b_6RRRt.__len__() > 0):
        partitionflag = True
        
    if (b_7wwWW.__len__() > 0):
        weatherflag = True
        
    b_iihVV = create_indicator_code(db_handle, table_name, statnr, date, time, debug_flag, partitionflag, weatherflag)
    
    synopstringBasic = b_IIIII  + \
        b_iihVV + \
        b_Nddff + \
        b_1sTTT + \
        b_2sTTT + \
        b_3PPPP + \
        b_4PPPP + \
        b_5PPPP + \
        b_6RRRt + \
        b_7wwWW + \
        b_8NChh

    if b_333_1xTTT != EMPTYSTRING or \
        b_333_2xTTT != EMPTYSTRING or \
        b_333_55SSS != EMPTYSTRING or \
        b_333_553SS != EMPTYSTRING or \
        b_333_910XX != EMPTYSTRING or \
        b_333_911XX != EMPTYSTRING or \
        b_333_912XX != EMPTYSTRING or \
        b_333_8NChh1 != EMPTYSTRING or \
        b_333_8NChh2 != EMPTYSTRING or \
        b_333_8NChh3 != EMPTYSTRING or \
        b_333_8NChh4 != EMPTYSTRING:
        synopstring333 = \
            DETAILBLOCK333 + \
            b_333_1xTTT + \
            b_333_2xTTT + \
            b_333_55SSS + \
            b_333_553SS + \
            b_333_8NChh1 + \
            b_333_8NChh2 + \
            b_333_8NChh3 + \
            b_333_8NChh4 + \
            b_333_910XX + \
            b_333_911XX + \
            b_333_912XX
    
    return synopstringBasic + synopstring333 + synopstring555