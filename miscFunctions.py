#!/usr/bin/env python
# -*- coding: utf-8 -*-

# miscFunctions.py
#
# purpose:  Wetterturnier.de
# author:   Szabolcs KÖLLÖ
# e-mail:   szabolcs.koelloe@uibk.ac.at
# web:      http://imgi.uibk.ac.at/
# created:  28-Aug-2013
# modified:

__author__ = 'szabo'

#import miscFunctions
import datetime

EMPTYSTRING = ""
SYNOP2TABLENAME = "synop2_imgi"
TAWES2TABLENAME = "tawes3_imgi"

# ---------------------------------------------------------------------------
# Parses the value and if value > 10000 return value is the flip of this value
# e.g. value = 10173 returnvalue=0173
# e.g. value = 9983 returnvalue is 9983
# ---------------------------------------------------------------------------
def flip_thousand(value, flipValue):
    if not (int(value) >= flipValue):
        return value

    return str(int(value) - flipValue)

# ---------------------------------------------------------------------------
# String check. in case of empty strings or blanks, returnvalue is false
# ---------------------------------------------------------------------------
def check_value_to_short(value, limit):
    value = str(value)
    if len(value.strip()) < limit:
        return False
    return True

# ---------------------------------------------------------------------------
# String check. in case of empty strings or blanks, returnvalue is false
# ---------------------------------------------------------------------------
def check_value_empty(value):
    value = str(value)
    if len(value.strip()) == 0:
        return False
    return True

# ---------------------------------------------------------------------------
# String check. in case of strings longer limit digits, returnvalue is false
# ---------------------------------------------------------------------------
def check_value_to_long(value, limit):
    value = str(value)
    if len(value.strip()) > limit:
        return False
    return True

# ---------------------------------------------------------------------------
# if a negative value is given, invert it. func return a string.
# ---------------------------------------------------------------------------
def invert_value(value):
    if int(value) < 0:
        value = int(value) * -1
        value = str(value)
    return value

# ---------------------------------------------------------------------------
# if value is negative return 1, otherwise 0
# if tablename and columnname are special give special sign
# ---------------------------------------------------------------------------
def create_sign(value, tableName=EMPTYSTRING, columnName=EMPTYSTRING):

    if tableName == TAWES2TABLENAME and columnName == "rf":
        return "9"

    if int(value) < 0:
        return "1"
    else:
        return "0"

# ---------------------------------------------------------------------------
# func adds leading zeros to create a value with the given length digits
# ---------------------------------------------------------------------------
def add_zeros(value, targetLength):
    leadingZeros = EMPTYSTRING
    for i in range (targetLength - len(str(value))):
        leadingZeros = "0" + leadingZeros

    return leadingZeros + str(value)

# ---------------------------------------------------------------------------
# creates a datetime.time object
# ---------------------------------------------------------------------------
def create_time(deliverytime):
    mytime = add_zeros(str(deliverytime), 4)
    return datetime.time(int(mytime[0:2]), int(mytime[2:4]), 0)