#!/usr/bin/env python
# -*- coding: utf-8 -*-

# dateFunctions.py
#
# purpose:  Wetterturnier.de
# author:   Szabolcs KÖLLÖ
# e-mail:   szabolcs.koelloe@uibk.ac.at
# web:      http://imgi.uibk.ac.at/
# created:  12-Aug-2013
# modified:

import datetime

__author__ = 'szabo'

WEEKDAYS = [0,1,2,3,4,5,6]
WEEKDAYSATURDAY = 6
WEEKDAYSUNDAY = 0
YEARCORRECTIONNUMBER = 1900

# ---------------------------------------------------------------------------
# create corrected string for last saturday
# year needs correction because the database stores 2013 as 113 (2013-1900=113)
# ---------------------------------------------------------------------------
def get_corrected_last_saturday(date):
    return correct_year(get_date(date, WEEKDAYSATURDAY))

# ---------------------------------------------------------------------------
# create corrected string for last sunday
# year needs correction because the database stores 2013 as 113 (2013-1900=113)
# ---------------------------------------------------------------------------
def get_corrected_last_sunday(date):
    return correct_year(get_date(date, WEEKDAYSUNDAY))

# ---------------------------------------------------------------------------
# create string for last saturday
# ---------------------------------------------------------------------------
def get_last_saturday(date):
    return get_date(date, WEEKDAYSATURDAY)

# ---------------------------------------------------------------------------
# create string for last sunday
# ---------------------------------------------------------------------------
def get_last_sunday(date):
    return get_date(date, WEEKDAYSUNDAY)

# ---------------------------------------------------------------------------
# get the right weekday with substraction
# ---------------------------------------------------------------------------
def get_date(date, weekday):
    for day in WEEKDAYS:
        newDate = date-datetime.timedelta(days=day)
        if int(newDate.strftime("%w")) == weekday:
            return newDate

# ---------------------------------------------------------------------------
# year needs correction because the database stores 2013 as 113 (2013-1900=113)
# ---------------------------------------------------------------------------
def correct_year(date):
    year = int(date.strftime("%Y"))
    return str(year - YEARCORRECTIONNUMBER) + date.strftime("%m%d")

# ---------------------------------------------------------------------------
# substracts one day form date
# ---------------------------------------------------------------------------
def get_one_day_before_date(date):
    return get_days_before_date(date, 1)

# ---------------------------------------------------------------------------
# adds one day to date
# ---------------------------------------------------------------------------
def get_one_day_after_date(date):
    return get_days_after_date(date, 1)

# ---------------------------------------------------------------------------
# substracts number days form date
# ---------------------------------------------------------------------------
def get_days_before_date(date, number_days):
    return (date - datetime.timedelta(days=number_days))

# ---------------------------------------------------------------------------
# adds number days to date
# ---------------------------------------------------------------------------
def get_days_after_date(date, number_days):
    return (date + datetime.timedelta(days=number_days))

