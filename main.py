#!/usr/bin/env /opt/python27/bin/python2.7
# -*- coding: utf-8 -*-

import os
import datetime
from datetime import timedelta

import sys
sys.path.append("/home/c707/c7071039/wetterturnier_hourly/")

USER = "innsbruck"
TARGETHOST = "prognose.met.fu-berlin.de"
#TARGETPATH = "/home/innsbruck/auswertung/incoming/obs/innsbruck_neu"
TARGETPATH = "/home/innsbruck/auswertung/incoming/obs"

OUTPUTFILENAME = "output"
#z.B. 14070512.obs
#today = datetime.datetime.today()
#OUTPUTFILENAME = today.strftime("%y%m%d%H")
OUTPUTFILENAMEEXT = ".txt"
OUTPUTPATH = "/home/c707/c7071039/wetterturnier_hourly/output"
NUMBEROURSTEPS = 6

BEGINLINE1 = "ZCZC"
BEGINLINE2 = "SMOS01"
BEGINLINE3 = "AAXX"
ENDLINE = "NNNN"

def checkline(dt, filename):
    returnvalue = True
    count = 0
    fobj = open(filename)
    for line in fobj:
        count += 1
        line = line.rstrip()
        if BEGINLINE1 in line:
            continue
        if BEGINLINE2 in line:
            continue
        if BEGINLINE3 in line:
            continue
        if ENDLINE in line:
            continue
        if line.split(" ").__len__() < 3:
            print "something is nasty with " + dt.strftime("%Y%m%d%H")
            print "in line: " + line + " returning False, nothing copying with scp"
            returnvalue = False               
    fobj.close()
    
    if count == 4:
        returnvalue = False
    
    return returnvalue

def createHourArray(numberHours):
    array = []
    today = datetime.datetime.today()
    for x in range (1, (numberHours+1)):
        temp = today - timedelta(hours=x)
        array.append(temp)
    return array
    
if __name__ == "__main__":
    array_with_dt = createHourArray(NUMBEROURSTEPS)
    
    for dt in array_with_dt:
        if os.path.exists(OUTPUTPATH + os.sep + dt.strftime("%Y%m%d%H") + OUTPUTFILENAMEEXT):
            continue
        
        createcommand = "/opt/python27/bin/python2.7 /home/c707/c7071039/wetterturnier_hourly/wetterturnier.py -d " \
                            + dt.strftime("%Y%m%d") + " -t " + dt.strftime("%H") + "00 -p " + OUTPUTPATH 
        print createcommand
        exitstatus = os.system(createcommand)
        oldfilename = OUTPUTPATH + os.sep + OUTPUTFILENAME + OUTPUTFILENAMEEXT
        if int(exitstatus) == 0:
            if checkline(dt, oldfilename):
                newfilename = OUTPUTPATH + os.sep + dt.strftime("%y%m%d%H") + ".obs"
                os.rename(oldfilename, newfilename)
                scpcommand = "/usr/bin/scp " + newfilename + " " + USER + "@" + TARGETHOST + ":" + TARGETPATH
                print scpcommand
                exitstatus = os.system(scpcommand)
                #print "exitstatus: %s" % exitstatus
            else:
                os.remove(oldfilename)
                
    #remove created files older then 1 day
    command = "/bin/find %s -type f -name \"*.txt\" -mtime +1 -exec /bin/rm -rf {} \;" % OUTPUTPATH
    print command
    os.system(command) 
