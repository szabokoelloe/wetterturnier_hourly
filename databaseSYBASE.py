#!/usr/bin/env /opt/python27/bin/python2.7
# -*- coding: utf-8 -*-

import sys
import sybpydb as db

__author__ = 'szabo'

def create_con(config):
    db_opts = {
        'host': config["host"],
        'user': config["user"],
        'pass': config["passw"],
    }

    try:
        con = db.connect(user=db_opts['user'],password=db_opts['pass'],servername=db_opts['host'])
    except Error, e:
        print "Error %s" % (e.args[0])
        sys.exit (1)

    return con